Author
======
Sergio Carracedo
info@sergiocarracedo.es
www.sergiocarracedo.es

Description
===========
This module search suspicious query strings, and block then responding 403
Mitigate https://www.drupal.org/sa-core-2018-002

Also you can mitigate in Apache .htaccess
using
# Mitigation for https://www.drupal.org/SA-CORE-2018-002
  RewriteCond %{QUERY_STRING} (.*)(23value|23default_value|element_parents=%23)(.*) [NC]
  RewriteCond %{REQUEST_METHOD} POST [NC]
  RewriteRule ^.*$  - [R=403,L]


Dependencies
=============
- None

Installation
=============
Enable module and clear caches

Disclamer
=============
This module isn't perfect, but mitigate common attack scripts

